<?php

namespace app\controllers;

use app\models\Partner;
use app\models\search\PartnerSearch;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class PartnerController
 * @package app\controllers
 */
class PartnerController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = Yii::createObject(PartnerSearch::class);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return string|Response
     */
    public function actionCreate()
    {
        $model = new Partner();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->addFlash('success', 'Record created');
            return $this->redirect(['index']);
        }
        return $this->render('create', [
            'model' => $model
        ]);
    }

    /**
     * @param int $id
     * @return string|Response
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->addFlash('success', 'Record updated');
            return $this->redirect(['index']);
        }
        return $this->render('update', [
            'model' => $model
        ]);
    }

    /**
     * @param int $id
     * @return Response
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    /**
     * @param int $id
     * @return Partner
     * @throws NotFoundHttpException
     */
    private function findModel($id)
    {
        $model = Partner::findOne($id);
        if ($model === null) {
            throw new NotFoundHttpException;
        }
        return $model;
    }
}
