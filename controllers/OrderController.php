<?php

namespace app\controllers;

use app\models\forms\Order;
use app\models\ProductOrder;
use app\models\search\ActiveOrdersSearch;
use app\models\search\DoneOrdersSearch;
use app\models\search\NewOrdersSearch;
use app\models\search\OutdatedOrdersSearch;
use app\services\EmailService;
use Exception;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UnprocessableEntityHttpException;

/**
 * Class OrderController
 * @package app\controllers
 */
class OrderController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = Yii::createObject(OutdatedOrdersSearch::class);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return string
     */
    public function actionActive()
    {
        $searchModel = Yii::createObject(ActiveOrdersSearch::class);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('active', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return string
     */
    public function actionNew()
    {
        $searchModel = Yii::createObject(NewOrdersSearch::class);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('new', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return string
     */
    public function actionDone()
    {
        $searchModel = Yii::createObject(DoneOrdersSearch::class);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('done', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return string|Response
     */
    public function actionCreate()
    {
        $model = new Order();
        $translaction = Yii::$app->db->beginTransaction();
        try {
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                $this>$this->saveRelations($model);
                $translaction->commit();
                Yii::$app->session->addFlash('success', 'Record updated');
                return $this->redirect(['update', 'id' => $model->primaryKey]);
            }
        } catch (Exception $ex) {
            $translaction->rollBack();
        }
        return $this->render('create', [
            'model' => $model
        ]);
    }

    /**
     * @param int $id
     * @return string|Response
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $translaction = Yii::$app->db->beginTransaction();
        try {
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                $this>$this->saveRelations($model);
                if ($model->status == Order::STATUS_DONE && !$model->email_sent) {
                    $model->email_sent = true;
                    $model->update(true, ['email_sent']);
                    $this->sendEmail($model);
                }
                $translaction->commit();
                Yii::$app->session->addFlash('success', 'Record updated');
                return $this->redirect(['update', 'id' => $model->primaryKey]);
            }
        } catch (Exception $ex) {
            $translaction->rollBack();
        }

        return $this->render('update', [
            'model' => $model,
            'totalPrice' => array_sum(array_map(function (ProductOrder $productOrder) {
                return $productOrder->price;
            }, $model->loadedProduts))
        ]);
    }

    /**
     * @param int $id
     * @return Response
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    public function actionChangeStatus()
    {
        if (!Yii::$app->request->isAjax) {
            throw new ForbiddenHttpException;
        }

        $post = Yii::$app->request->post('Order');
        if ($post) {
            $model = $this->findModel(ArrayHelper::getValue($post, 'id'));
            $model->status = ArrayHelper::getValue($post, 'status');
            if ($model->update(true, ['status']) === false) {
                throw new UnprocessableEntityHttpException($model->getFirstError('status'));
            }

            if ($model->status == Order::STATUS_DONE && !$model->email_sent) {
                $model->email_sent = true;
                $model->update(true, ['email_sent']);
                $this->sendEmail($model);
            }
            return true;
        }
        return false;
    }

    /**
     * @param int $id
     * @return Order
     * @throws NotFoundHttpException
     */
    private function findModel($id)
    {
        $model = Order::findOne($id);
        if ($model === null) {
            throw new NotFoundHttpException;
        }
        return $model;
    }

    /**
     * @param Order $model
     */
    private function sendEmail(Order $model)
    {
        $service = new EmailService();
        $service->sendOrderEmail($model);
    }

    /**
     * @param Order $model
     */
    private function saveRelations(Order $model)
    {
        ProductOrder::deleteAll([
            'order_id' => $model->primaryKey
        ]);
        foreach ($model->loadedProduts as $product) {
            $product->order_id = $model->primaryKey;
            $product->price = $product->quantity * $product->product->price;
            $product->save();
        }
    }
}
