<?php

namespace app\controllers;

use app\models\search\VendorSearch;
use app\models\Vendor;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class VendorController
 * @package app\controllers
 */
class VendorController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = Yii::createObject(VendorSearch::class);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return string|Response
     */
    public function actionCreate()
    {
        $model = new Vendor();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->addFlash('success', 'Record created');
            return $this->redirect(['index']);
        }
        return $this->render('create', [
            'model' => $model
        ]);
    }

    /**
     * @param int $id
     * @return string|Response
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->addFlash('success', 'Record updated');
            return $this->redirect(['index']);
        }
        return $this->render('update', [
            'model' => $model
        ]);
    }

    /**
     * @param int $id
     * @return Response
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    /**
     * @param int $id
     * @return Vendor
     * @throws NotFoundHttpException
     */
    private function findModel($id)
    {
        $model = Vendor::findOne($id);
        if ($model === null) {
            throw new NotFoundHttpException;
        }
        return $model;
    }
}
