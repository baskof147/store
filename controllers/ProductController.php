<?php

namespace app\controllers;

use app\models\Product;
use app\models\search\ProductSearch;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UnprocessableEntityHttpException;

/**
 * Class ProductController
 * @package app\controllers
 */
class ProductController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = Yii::createObject(ProductSearch::class);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return string|Response
     */
    public function actionCreate()
    {
        $model = new Product();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->addFlash('success', 'Record created');
            return $this->redirect(['index']);
        }
        return $this->render('create', [
            'model' => $model
        ]);
    }

    /**
     * @param int $id
     * @return string|Response
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->addFlash('success', 'Record updated');
            return $this->redirect(['index']);
        }
        return $this->render('update', [
            'model' => $model
        ]);
    }

    /**
     * @param int $id
     * @return Response
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    /**
     * @return float
     * @throws ForbiddenHttpException
     * @throws UnprocessableEntityHttpException
     */
    public function actionChangePrice()
    {
        if (!Yii::$app->request->isAjax) {
            throw new ForbiddenHttpException;
        }

        $product = Yii::$app->request->post('Product');
        if ($product) {
            $model = $this->findModel(ArrayHelper::getValue($product, 'id'));
            $model->price = ArrayHelper::getValue($product, 'price');
            if ($model->update(true, ['price']) === false) {
                throw new UnprocessableEntityHttpException($model->getFirstError('price'));
            }
            return true;
        }
        return false;
    }

    /**
     * @param int $id
     * @return Product
     * @throws NotFoundHttpException
     */
    private function findModel($id)
    {
        $model = Product::findOne($id);
        if ($model === null) {
            throw new NotFoundHttpException;
        }
        return $model;
    }
}
