<?php

use app\models\Product;
use app\models\Vendor;
use app\models\search\ProductSearch;
use kartik\editable\Editable;
use yii\data\ActiveDataProvider;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var ActiveDataProvider $dataProvider
 * @var ProductSearch $searchModel
 */

$this->title = Yii::t('app', 'Products list');
$this->params['breadcrumbs'][] = $this->title;

?>
<?= Html::a('<i class="fa fa-plus"></i> ' . Yii::t('app', 'Add product'), ['create'], [
    'class' => 'btn btn-success'
]) ?>
<div class="box box-primary" style="margin-top:25px;">
    <div class="box-body">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                [
                    'attribute' => 'vendor_id',
                    'filter' => ArrayHelper::map(Vendor::find()->all(), 'id', 'name'),
                    'value' => function (Product $model) {
                        return $model->vendor->name;
                    }
                ],
                'name',
                [
                    'attribute' => 'price',
                    'format' => 'raw',
                    'value' => function (Product $model) {
                        return Editable::widget([
                            'name' => 'Product[price]',
                            'additionalData' => [
                                'Product[id]' => $model->primaryKey
                            ],
                            'value' => $model->price,
                            'size'=>'md',
                            'options' => ['class'=>'form-control', 'placeholder'=> Yii::t('app', 'Enter price')],
                            'ajaxSettings' => [
                                'url' => Url::to(['product/change-price']),
                            ]
                        ]);
                    }
                ],
                [
                    'class' => ActionColumn::class,
                    'headerOptions' => [
                        'width' => 50,
                    ],
                    'template' => '{update} {delete}',
                ],
            ],
        ]) ?>
    </div>
</div>
