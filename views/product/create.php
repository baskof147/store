<?php

use app\models\Product;
use yii\web\View;

/**
 * @var Product $model
 * @var View $this
 */


$this->title = Yii::t('app', 'Create product');
$this->params['breadcrumbs'][] = [
    'url' => ['index'],
    'label' => Yii::t('app', 'Products list')
];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box box-primary">
    <div class="box-body">
        <?= $this->render('_form', [
            'model' => $model
        ]) ?>
    </div>
</div>
