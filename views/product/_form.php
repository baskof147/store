<?php

/**
 * @var Product $model
 * @var View $this
 */

use app\models\Product;
use app\models\Vendor;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

$form = ActiveForm::begin();
echo $form->field($model, 'name');
echo $form->field($model, 'vendor_id')
    ->dropDownList(ArrayHelper::map(Vendor::find()->all(), 'id', 'name'));
echo $form->field($model, 'price');
echo Html::submitButton(Yii::t('app', 'Save'), [
    'class' => 'btn btn-success'
]);
$form->end();