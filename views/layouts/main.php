<?php

use app\assets\AdminLTEAsset;
use app\assets\AppAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use yii\widgets\Menu;

/**
 * @var string $content
 */

AppAsset::register($this);
$adminLteAsset = AdminLTEAsset::register($this);

$this->beginPage();
?>
    <!DOCTYPE html>
    <html>
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta charset="<?= Yii::$app->charset ?>">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="hold-transition skin-blue-light sidebar-mini">
    <?php $this->beginBody(); ?>
    <div class="wrapper">
        <header class="main-header">

            <!-- Logo -->
            <a data-toggle="offcanvas" role="button" class="logo">
                <!-- mini logo for sidebar mini 50x50 pixels -->
                <span class="logo-mini"><i class="fa fa-bars"></i></span>
                <!-- logo for regular state and mobile devices -->
                <span class="logo-lg"><?= Yii::t('app', 'Store') ?></span>
            </a>

            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Navbar Right Menu -->
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <?= Html::img($adminLteAsset->baseUrl . '/img/avatar5.png', ['class' => 'user-image']) ?>
                                <span class="hidden-xs"><?= Yii::$app->user->identity->email ?></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header">
                                    <?= Html::img($adminLteAsset->baseUrl . '/img/avatar5.png', ['class' => 'img-circle']) ?>
                                    <p>
                                        <?= Yii::$app->user->identity->email ?>
                                    </p>
                                </li>
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-right">
                                        <a href="<?=Url::to(['/site/logout'])?>" class="btn btn-default btn-flat">Logout</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>

            </nav>
        </header>
        <!-- Left side column. contains the logo and sidebar -->
        <aside class="main-sidebar">
            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">
                <!-- sidebar menu: : style can be found in sidebar.less -->
                <?= Menu::widget([
                    'encodeLabels' => false,
                    'options' => [
                        'class' => 'sidebar-menu',
                    ],
                    'activateParents' => true,
                    'submenuTemplate' => "\n<ul class='treeview-menu'>\n{items}\n</ul>\n",
                    'items' => [
                        [
                            'label' => '<i class="fa fa-user"></i> <span>Vendors</span>',
                            'url' => ['/vendor/index'],
                            'active' => Yii::$app->controller->id === 'vendor',
                        ],
                        [
                            'label' => '<i class="fa fa-database"></i> <span>Partners</span>',
                            'url' => ['/partner/index'],
                            'active' => Yii::$app->controller->id === 'partner',
                        ],
                        [
                            'label' => '<i class="fa fa-envelope"></i> <span>Products</span>',
                            'url' => ['/product/index'],
                            'active' => Yii::$app->controller->id === 'product',
                        ],
                        [
                            'label' => '<i class="fa fa-flag"></i> <span>Orders</span>',
                            'url' => ['/order/index'],
                            'active' => Yii::$app->controller->id === 'order',
                        ],
                    ],
                ])?>
            </section>
            <!-- /.sidebar -->
        </aside>
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <?php if (isset($this->params['breadcrumbs'])): ?>
                    <?= Breadcrumbs::widget([
                        'options' => [
                            'class' => 'breadcrumb',
                        ],
                        'encodeLabels' => false,
                        'homeLink' => [
                            'label' => '<i class="fa fa-dashboard"></i> ' . Yii::t('app', 'Store'),
                            'url' => ['/site/index'],
                        ],
                        'links' => $this->params['breadcrumbs'],
                    ]) ?>
                <?php endif; ?>
            </section>

            <!-- Main content -->
            <section class="content" style="padding-top: 35px;">
                <?php
                if (count(Yii::$app->session->getAllFlashes())) {
                    foreach (Yii::$app->session->getAllFlashes() as $key => $messages) {
                        foreach ($messages as $message) {
                            echo '<div class="callout callout-' . $key . '"><p>' . $message . '</p></div>';
                        }
                    }
                }
                ?>
                <?= $content ?>
            </section>
        </div>
    </div>
    <?php $this->endBody(); ?>
    </body>
    </html>
<?php
$this->endPage();
