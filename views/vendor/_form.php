<?php

/**
 * @var Vendor $model
 * @var View $this
 */

use app\models\Vendor;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

$form = ActiveForm::begin();
echo $form->field($model, 'name');
echo $form->field($model, 'email');
echo Html::submitButton(Yii::t('app', 'Save'), [
    'class' => 'btn btn-success'
]);
$form->end();