<?php

use app\models\Vendor;
use yii\web\View;

/**
 * @var Vendor $model
 * @var View $this
 */


$this->title = Yii::t('app', 'Create vendor');
$this->params['breadcrumbs'][] = [
    'url' => ['index'],
    'label' => Yii::t('app', 'Vendors list')
];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box box-primary">
    <div class="box-body">
        <?= $this->render('_form', [
            'model' => $model
        ]) ?>
    </div>
</div>
