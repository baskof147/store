<?php

use app\models\search\PartnerSearch;
use yii\data\ActiveDataProvider;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * @var ActiveDataProvider $dataProvider
 * @var PartnerSearch $searchModel
 */

$this->title = Yii::t('app', 'Partners list');
$this->params['breadcrumbs'][] = $this->title;

?>
<?= Html::a('<i class="fa fa-plus"></i> ' . Yii::t('app', 'Add partner'), ['create'], [
    'class' => 'btn btn-success'
]) ?>
<div class="box box-primary" style="margin-top:25px;">
    <div class="box-body">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                'name',
                'email',
                [
                    'class' => ActionColumn::class,
                    'headerOptions' => [
                        'width' => 50,
                    ],
                    'template' => '{update} {delete}',
                ],
            ],
        ]) ?>
    </div>
</div>
