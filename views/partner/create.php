<?php

use app\models\Partner;
use yii\web\View;

/**
 * @var Partner $model
 * @var View $this
 */


$this->title = Yii::t('app', 'Create partner');
$this->params['breadcrumbs'][] = [
    'url' => ['index'],
    'label' => Yii::t('app', 'Partners list')
];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box box-primary">
    <div class="box-body">
        <?= $this->render('_form', [
            'model' => $model
        ]) ?>
    </div>
</div>
