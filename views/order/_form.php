<?php

use app\models\forms\Order;
use app\models\Partner;
use app\models\Product;
use app\models\ProductOrder;
use backend\modules\templates\models\EmailTemplateView;
use kartik\datetime\DateTimePicker;
use unclead\multipleinput\TabularInput;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;

/**
 * @var Order $model
 * @var View $this
 */

$form = ActiveForm::begin();
echo $form->field($model, 'partner_id')
    ->dropDownList(ArrayHelper::map(Partner::find()->all(), 'id', 'name'));
echo $form->field($model, 'client_email');
echo $form->field($model, 'status')->dropDownList(Order::getStatusList());
echo $form->field($model, 'delivery_at')->widget(DateTimePicker::class, [
    'pluginOptions' => [
        'todayHighlight' => true
    ]
]);
echo TabularInput::widget([
    'models' => $model->loadedProduts,
    'modelClass' => ProductOrder::class,
    'form' => $form,
    'enableError' => true,
    'cloneButton' => true,
    'columns' => [
        [
            'title' => Yii::t('app', 'Product'),
            'name' => 'product_id',
            'type' => 'dropDownList',
            'items' => ArrayHelper::map(Product::find()->all(), 'id', 'name'),
        ],
        [
            'title' => Yii::t('app', 'Quantity'),
            'name' => 'quantity',
            'defaultValue' => 1,
            'options' => [
                'type' => 'number',
                'class' => 'quantity-field',
                'min' => 1,
                'onkeypress' => 'return event.charCode >= 48',
            ]
        ],
    ]
]);
echo Html::submitButton(Yii::t('app', 'Save'), [
    'class' => 'btn btn-success'
]);
$form->end();


