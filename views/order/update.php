<?php

use app\models\forms\Order;
use yii\helpers\ArrayHelper;
use yii\web\View;

/**
 * @var Order $model
 * @var View $this
 * @var float $totalPrice
 */


$this->title = Yii::t('app', 'Update order');
$this->params['breadcrumbs'][] = [
    'url' => ['index'],
    'label' => Yii::t('app', 'Orders list')
];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box box-primary">
    <div class="box-body">
        <?= $this->render('_form', [
            'model' => $model
        ]) ?>
    </div>
</div>
<div class="box box-primary">
    <div class="box-body">
        <?= Yii::t('app', 'Total price: {price}', [
            'price' => Yii::$app->formatter->asCurrency($totalPrice, ArrayHelper::getValue(Yii::$app->params, 'currency'))
        ])?>
    </div>
</div>