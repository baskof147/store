<?php

use app\models\Product;
use app\models\search\ActiveOrdersSearch;
use app\models\Vendor;
use app\models\search\ProductSearch;
use kartik\editable\Editable;
use yii\data\ActiveDataProvider;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var ActiveDataProvider $dataProvider
 * @var ActiveOrdersSearch $searchModel
 */

$this->title = Yii::t('app', 'Active orders');
$this->params['breadcrumbs'][] = $this->title;

?>
<?= Html::a('<i class="fa fa-plus"></i> ' . Yii::t('app', 'Add order'), ['create'], [
    'class' => 'btn btn-success'
]) ?>
<?= $this->render('_menu') ?>
<?= $this->render('_grid', [
    'searchModel' => $searchModel,
    'dataProvider' => $dataProvider,
]) ?>
