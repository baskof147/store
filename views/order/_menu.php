<?php

use yii\bootstrap\Tabs;

echo Tabs::widget([
    'options' => [
        'style' => 'margin-top: 25px;'
    ],
    'items' => [
        [
            'label' => Yii::t('app', 'Outdated'),
            'url' => ['index'],
            'active' => Yii::$app->controller->action->id === 'index'
        ],
        [
            'label' => Yii::t('app', 'Active'),
            'url' => ['active'],
            'active' => Yii::$app->controller->action->id === 'active'
        ],
        [
            'label' => Yii::t('app', 'New'),
            'url' => ['new'],
            'active' => Yii::$app->controller->action->id === 'new'
        ],
        [
            'label' => Yii::t('app', 'Done'),
            'url' => ['done'],
            'active' => Yii::$app->controller->action->id === 'done'
        ],
    ],
]);
