<?php

use app\models\Order;
use app\models\Partner;
use app\models\search\OutdatedOrdersSearch;
use kartik\date\DatePicker;
use kartik\editable\Editable;
use yii\data\ActiveDataProvider;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/**
 * @var ActiveDataProvider $dataProvider
 * @var OutdatedOrdersSearch $searchModel
 */

 ?>
<div class="box box-primary">
    <div class="box-body">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                [
                    'attribute' => 'partner_id',
                    'filter' => ArrayHelper::map(Partner::find()->all(), 'id', 'name'),
                    'value' => function (Order $model) {
                        return $model->partner->name;
                    }
                ],
                'client_email',
                [
                    'attribute' => 'status',
                    'format' => 'raw',
                    'filter' => Order::getStatusList(),
                    'value' => function (Order $model) {
                        return Editable::widget([
                            'name' => 'Order[status]',
                            'additionalData' => [
                                'Order[id]' => $model->primaryKey
                            ],
                            'value' => $model->status,
                            'asPopover' => false,
                            'header' => 'Status',
                            'inputType' => Editable::INPUT_DROPDOWN_LIST,
                            'data' => Order::getStatusList(),
                            'options' => [
                                'class' => 'form-control'
                            ],
                            'displayValueConfig'=> Order::getStatusList(),
                            'ajaxSettings' => [
                                'url' => Url::to(['order/change-status']),
                            ]
                        ]);
                    }
                ],
                [
                    'attribute' => 'delivery_at',
                    'filter' => DatePicker::widget([
                        'model' => $searchModel,
                        'attribute' => 'delivery_at',
                        'pluginOptions' => [
                            'format' => 'yyyy-mm-dd',
                            'todayHighlight' => true
                        ]
                    ])
                ],
                [
                    'class' => ActionColumn::class,
                    'headerOptions' => [
                        'width' => 50,
                    ],
                    'template' => '{update} {delete}',
                ],
            ],
        ]) ?>
    </div>
</div>
