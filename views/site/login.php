<?php

use app\models\LoginForm;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/**
 * @var LoginForm $model
 * @var View $this
 */
?>
<div class="user_login">
    <div class="login-box">
        <div class="login-box-body">
            <?php $form = ActiveForm::begin(); ?>
            <?= $form->field($model, 'email') ?>
            <?= $form->field($model, 'password')->passwordInput() ?>
            <?= $form->field($model, 'rememberMe')->checkbox() ?>
            <?= Html::submitButton(Yii::t('app', 'Login'), [
                'class' => 'btn btn-success'
            ]) ?>
            <?php $form->end(); ?>
        </div>
    </div>
</div>
