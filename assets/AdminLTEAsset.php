<?php

namespace app\assets;

use yii\bootstrap\BootstrapAsset;
use yii\bootstrap\BootstrapPluginAsset;
use yii\web\AssetBundle;

/**
 * Class AdminLTEAsset
 * @package app\assets
 */
class AdminLTEAsset extends AssetBundle
{
    /**
     * @inheritdoc
     */
    public $sourcePath = '@bower/admin-lte/dist';

    /**
     * @inheritdoc
     */
    public $css = [
        'css/AdminLTE.min.css',
        'css/skins/skin-blue-light.min.css',
    ];

    /**
     * @inheritdoc
     */
    public $depends = [
        BootstrapAsset::class,
        BootstrapPluginAsset::class,
    ];
}
