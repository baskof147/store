<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Class AppAsset
 * @package app\assets
 */
class AppAsset extends AssetBundle
{
    /**
     * @inheritdoc
     */
    public $depends = [
        FontAwesomeAsset::class,
        AdminLTEAsset::class,
    ];
}
