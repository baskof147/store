<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Class FontAwesomeAsset
 * @package app\assets
 */
class FontAwesomeAsset extends AssetBundle
{
    /**
     * @inheritdoc
     */
    public $css = [
        '//use.fontawesome.com/releases/v5.3.1/css/all.css',
    ];
}
