<?php

use yii\db\Expression;

return [
    [
        'id' => 1,
        'name' => 'First partner',
        'email' => 'firstpartner@gmail.com',
        'created_at' => new Expression('now()'),
        'updated_at' => new Expression('now()'),
    ],
    [
        'id' => 2,
        'name' => 'Second partner',
        'email' => 'secondpartner@gmail.com',
        'created_at' => new Expression('now()'),
        'updated_at' => new Expression('now()'),
    ],
];
