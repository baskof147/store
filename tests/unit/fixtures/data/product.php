<?php

use yii\db\Expression;

return [
    [
        'vendor_id' => 1,
        'name' => 'Samsung TV',
        'price' => 550,
        'created_at' => new Expression('now()'),
        'updated_at' => new Expression('now()'),
    ],
    [
        'vendor_id' => 1,
        'name' => 'Dexp keyboard',
        'price' => 12,
        'created_at' => new Expression('now()'),
        'updated_at' => new Expression('now()'),
    ],
    [
        'vendor_id' => 2,
        'name' => 'Pro mouse',
        'price' => 25,
        'created_at' => new Expression('now()'),
        'updated_at' => new Expression('now()'),
    ],
    [
        'vendor_id' => 2,
        'name' => 'Philips monitor',
        'price' => 220,
        'created_at' => new Expression('now()'),
        'updated_at' => new Expression('now()'),
    ],
];
