<?php

use yii\db\Expression;

return [
    [
        'id' => 1,
        'partner_id' => 1,
        'client_email' => 'clientemail@gmail.com',
        'delivery_at' => date('Y-m-d h:i:s'),
        'status' => 20,
        'created_at' => new Expression('now()'),
        'updated_at' => new Expression('now()'),
    ],
    [
        'id' => 2,
        'partner_id' => 1,
        'client_email' => 'clientemail@gmail.com',
        'delivery_at' => date('Y-m-d h:i:s', time() - 86400),
        'status' => 20,
        'created_at' => date('Y-m-d', time() - (86400 * 2)),
        'updated_at' => date('Y-m-d', time() - (86400 * 2)),
    ],
    [
        'id' => 3,
        'partner_id' => 2,
        'client_email' => 'clientemail@gmail.com',
        'delivery_at' => date('Y-m-d h:i:s', time() - 86400),
        'status' => 10,
        'created_at' => date('Y-m-d', time() - (86400 * 2)),
        'updated_at' => date('Y-m-d', time() - (86400 * 2)),
    ],
    [
        'id' => 4,
        'partner_id' => 2,
        'client_email' => 'clientemail@gmail.com',
        'delivery_at' => date('Y-m-d h:i:s', time() + 80400),
        'status' => 10,
        'created_at' => date('Y-m-d', time() - (86400 * 2)),
        'updated_at' => date('Y-m-d', time() - (86400 * 2)),
    ],
    [
        'id' => 5,
        'partner_id' => 2,
        'client_email' => 'clientemail@gmail.com',
        'delivery_at' => date('Y-m-d h:i:s', time() + 86400),
        'status' => 0,
        'created_at' => date('Y-m-d', time() - 86400),
        'updated_at' => date('Y-m-d', time() - 86400),
    ],
];
