<?php

use yii\db\Expression;

return [
    [
        'id' => 1,
        'name' => 'First vendor',
        'email' => 'firstvendor@gmail.com',
        'created_at' => new Expression('now()'),
        'updated_at' => new Expression('now()'),
    ],
    [
        'id' => 2,
        'name' => 'Second vendor',
        'email' => 'secondvendor@gmail.com',
        'created_at' => new Expression('now()'),
        'updated_at' => new Expression('now()'),
    ],
];
