<?php

use yii\db\Expression;

return [
    [
        'email' => 'admin@gmail.com',
        'password_hash' => Yii::$app->security->generatePasswordHash('admin'),
        'created_at' => new Expression('now()'),
        'updated_at' => new Expression('now()'),
    ]
];
