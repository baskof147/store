<?php

namespace tests\unit\fixtures;

use app\models\Vendor;
use yii\test\ActiveFixture;

/**
 * Class VendorFixture
 * @package unit\fixtures
 */
class VendorFixture extends ActiveFixture
{
    /**
     * @inheritdoc
     */
    public $modelClass = Vendor::class;
}
