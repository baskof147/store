<?php

namespace tests\unit\fixtures;

use app\models\Order;
use yii\test\ActiveFixture;

/**
 * Class OrderFixture
 * @package unit\fixtures
 */
class OrderFixture extends ActiveFixture
{
    /**
     * @inheritdoc
     */
    public $modelClass = Order::class;
}
