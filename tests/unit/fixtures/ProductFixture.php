<?php

namespace tests\unit\fixtures;

use app\models\Product;
use yii\test\ActiveFixture;

/**
 * Class ProductFixture
 * @package unit\fixtures
 */
class ProductFixture extends ActiveFixture
{
    /**
     * @inheritdoc
     */
    public $modelClass = Product::class;
}
