<?php

namespace tests\unit\fixtures;

use app\models\Partner;
use yii\test\ActiveFixture;

/**
 * Class PartnerFixture
 * @package unit\fixtures
 */
class PartnerFixture extends ActiveFixture
{
    /**
     * @inheritdoc
     */
    public $modelClass = Partner::class;
}
