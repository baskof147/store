<?php

namespace tests\unit\fixtures;

use app\models\User;
use yii\test\ActiveFixture;

/**
 * Class UserFixture
 * @package unit\fixtures
 */
class UserFixture extends ActiveFixture
{
    /**
     * @inheritdoc
     */
    public $modelClass = User::class;
}