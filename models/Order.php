<?php

namespace app\models;

use app\models\query\OrderQuery;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * @property int $id
 * @property int $partner_id
 * @property int $status
 * @property string $client_email
 * @property string|null $delivery_at
 * @property string $created_at
 * @property string $updated_at
 * @property bool $email_sent
 *
 * @property-read Partner $partner
 * @property-read ProductOrder[] $productOrders
 * @property-read string|null orderStatus
 */
class Order extends ActiveRecord
{
    const STATUS_NEW = 0;
    const STATUS_CONFIRMED = 10;
    const STATUS_DONE = 20;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%order}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['status', 'default', 'value' => self::STATUS_NEW],
            ['client_email', 'trim'],
            ['client_email', 'email'],
            [['partner_id', 'client_email', 'delivery_at'], 'required'],
            [['partner_id', 'status'], 'integer'],
            [['delivery_at'], 'safe'],
            [['email_sent'], 'boolean'],
            [['delivery_at'], 'filter', 'filter' => function () {
                return date('Y-m-d H:i:s', strtotime($this->delivery_at));
            }],
            [['client_email'], 'string', 'max' => 255],
            [['partner_id'], 'exist', 'skipOnError' => true, 'targetRelation' => 'partner'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'partner_id' => Yii::t('app', 'Partner'),
            'status' => Yii::t('app', 'Status'),
            'client_email' => Yii::t('app', 'Client email'),
            'delivery_at' => Yii::t('app', 'Delivery date'),
            'created_at' => Yii::t('app', 'Create date'),
            'updated_at' => Yii::t('app', 'Update date'),
        ];
    }

    /**
     * @return array
     */
    public static function getStatusList()
    {
        return [
            self::STATUS_NEW => Yii::t('app', 'New'),
            self::STATUS_CONFIRMED => Yii::t('app', 'Confirmed'),
            self::STATUS_DONE => Yii::t('app', 'Done'),
        ];
    }

    /**
     * @return string|null
     */
    public function getOrderStatus()
    {
        return ArrayHelper::getValue(self::getStatusList(), $this->status);
    }

    /**
     * @return ActiveQuery
     */
    public function getPartner()
    {
        return $this->hasOne(Partner::class, ['id' => 'partner_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getProductOrders()
    {
        return $this->hasMany(ProductOrder::class, ['order_id' => 'id']);
    }

    /**
     * @return OrderQuery
     */
    public static function find()
    {
        return new OrderQuery(get_called_class());
    }
}
