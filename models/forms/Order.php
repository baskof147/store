<?php

namespace app\models\forms;

use app\models\ProductOrder;
use backend\modules\templates\models\EmailTemplateView;
use yii\helpers\ArrayHelper;

/**
 * Class Order
 * @package app\models\forms
 */
class Order extends \app\models\Order
{
    /**
     * @var array
     */
    public $loadedProduts = [];

    /**
     * @inheritDoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            ['loadedProduts', 'validateLoadedViews'],
        ]);
    }

    /**
     * Load template and related views data
     * @inheritDoc
     */
    public function load($data, $formName = null)
    {
        if (!parent::load($data, $formName)) {
            return false;
        }

        $this->loadedProduts = [];
        foreach (ArrayHelper::getValue($data, 'ProductOrder', []) as $index => $viewData) {
            $this->loadedProduts[$index] = new ProductOrder();
        }

        return self::loadMultiple($this->loadedProduts, $data);
    }

    /**
     * Validate each loaded view and total percentage
     */
    public function validateLoadedViews()
    {
        if (!self::validateMultiple($this->loadedProduts)) {
            $this->addError('loadedViews', 'Products have errors');
            return;
        }
    }

    /**
     * @inheritDoc
     */
    public function afterFind()
    {
        parent::afterFind();
        $this->loadedProduts = $this->productOrders;
    }
}
