<?php

namespace app\models\query;

use app\models\Order;
use common\models\ApiClient;
use common\models\queries\ApiClientQuery;
use DateTime;
use yii\db\ActiveQuery;
use yii\db\Expression;

/**
 * Class OrderQuery
 * @package app\models\query
 */
class OrderQuery extends ActiveQuery
{
    /**
     * @inheritdoc
     * @return array|null|Order
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @inheritdoc
     * @return array|Order[]
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @param int $status
     * @return OrderQuery
     */
    public function byStatus($status)
    {
        return $this->andWhere([
            'status' => $status
        ]);
    }

    /**
     * @return OrderQuery
     */
    public function outdated()
    {
        return $this->andWhere([
            '<', 'delivery_at', new Expression('now()')
        ]);
    }

    /**
     * @param DateTime $date
     * @return OrderQuery
     */
    public function byDateMoreThan(DateTime $date)
    {
        return $this->andWhere([
            '>=', 'delivery_at', $date->format(DATE_RFC3339)
        ]);
    }

    /**
     * @param DateTime $from
     * @param DateTime $to
     * @return OrderQuery
     */
    public function byDateBetween(DateTime $from, DateTime $to)
    {
        return $this->andWhere([
            'AND',
            ['>=', 'delivery_at', $from->format(DATE_RFC3339)],
            ['<=', 'delivery_at', $to->format(DATE_RFC3339)]
        ]);
    }

    /**
     * @return OrderQuery
     */
    public function today()
    {
        $date = new DateTime();
        return $this->andWhere([
            'AND',
            ['>=', 'delivery_at', $date->format('Y-m-d 00:00:00')],
            ['<=', 'delivery_at', $date->format('Y-m-d 23:59:59')]
        ]);
    }
}
