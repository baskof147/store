<?php


namespace app\models\search;

use app\models\Order;
use DateTime;
use DateInterval;
use yii\db\ActiveQuery;

/**
 * Class DoneOrdersSearch
 * @package app\models\search
 */
class DoneOrdersSearch extends AbstractOrderSearch
{
    private const QUERY_LIMIT = 50;
    /**
     * @inheritDoc
     */
    public function getQuery(): ActiveQuery
    {
        $date = (new DateTime())->add(DateInterval::createFromDateString('-1 day'));
        return Order::find()
            ->byStatus(Order::STATUS_DONE)
            ->today()
            ->limit(self::QUERY_LIMIT)
        ;
    }

    /**
     * @inheritDoc
     */
    public function getSort(): array
    {
        return [
            'defaultOrder' => [
                'delivery_at' => SORT_DESC
            ]
        ];
    }
}
