<?php


namespace app\models\search;


use app\models\Order;
use yii\db\ActiveQuery;

/**
 * Class OutdatedOrdersSearch
 * @package app\models\search
 */
class OutdatedOrdersSearch extends AbstractOrderSearch
{
    /**
     * @inheritDoc
     */
    public function getQuery(): ActiveQuery
    {
        return Order::find()
            ->byStatus(Order::STATUS_CONFIRMED)
            ->outdated()
        ;
    }

    /**
     * @inheritDoc
     */
    public function getSort(): array
    {
        return [
            'defaultOrder' => [
                'delivery_at' => SORT_DESC
            ]
        ];
    }
}
