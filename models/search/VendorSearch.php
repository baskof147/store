<?php

namespace app\models\search;

use app\models\Vendor;
use common\models\Message;
use Yii;
use yii\db\ActiveRecord;

/**
 * Class VendorSearch
 * @package app\models\search
 */
class VendorSearch extends AbstractPersonSearch
{
    /**
     * @inheritDoc
     */
    public function getOwner(): ActiveRecord
    {
        return Yii::createObject(Vendor::class);
    }
}
