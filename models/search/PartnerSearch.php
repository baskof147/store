<?php

namespace app\models\search;

use app\models\Partner;
use common\models\Message;
use Yii;
use yii\db\ActiveRecord;

/**
 * Class PartnerSearch
 * @package app\models\search
 */
class PartnerSearch extends AbstractPersonSearch
{
    /**
     * @inheritDoc
     */
    public function getOwner(): ActiveRecord
    {
        return Yii::createObject(Partner::class);
    }
}
