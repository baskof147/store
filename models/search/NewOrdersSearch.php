<?php


namespace app\models\search;

use app\models\Order;
use DateTime;
use yii\db\ActiveQuery;

/**
 * Class NewOrdersSearch
 * @package app\models\search
 */
class NewOrdersSearch extends AbstractOrderSearch
{
    /**
     * @inheritDoc
     */
    public function getQuery(): ActiveQuery
    {
        return Order::find()
            ->byStatus(Order::STATUS_NEW)
            ->byDateMoreThan(new DateTime())
        ;
    }

    /**
     * @inheritDoc
     */
    public function getSort(): array
    {
        return [
            'defaultOrder' => [
                'delivery_at' => SORT_ASC
            ]
        ];
    }
}
