<?php


namespace app\models\search;

use app\models\Order;
use DateTime;
use DateInterval;
use yii\db\ActiveQuery;

/**
 * Class ActiveOrdersSearch
 * @package app\models\search
 */
class ActiveOrdersSearch extends AbstractOrderSearch
{
    /**
     * @inheritDoc
     */
    public function getQuery(): ActiveQuery
    {
        $dateFrom = new DateTime();
        $dateTo = (new DateTime())->add(DateInterval::createFromDateString('+24 hour'));
        return Order::find()
            ->byStatus(Order::STATUS_CONFIRMED)
            ->byDateBetween($dateFrom, $dateTo)
        ;
    }

    /**
     * @inheritDoc
     */
    public function getSort(): array
    {
        return [
            'defaultOrder' => [
                'delivery_at' => SORT_ASC
            ]
        ];
    }
}
