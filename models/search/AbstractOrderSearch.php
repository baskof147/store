<?php


namespace app\models\search;

use app\models\Product;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

/**
 * Class OrderSearch
 * @package app\models\search
 */
abstract class AbstractOrderSearch extends Model
{
    /**
     * @var string
     */
    public $client_email;

    /**
     * @var int
     */
    public $partner_id;

    /**
     * @var int
     */
    public $status;

    /**
     * @var string
     */
    public $delivery_at;

    /**
     * @return ActiveQuery
     */
    abstract public function getQuery(): ActiveQuery;

    /**
     * @return array
     */
    abstract public function getSort(): array;

    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [
            [['client_email'], 'trim'],
            [['partner_id'], 'integer'],
            [['status'], 'integer'],
            [['delivery_at'], 'safe']
        ];
    }

    /**
     * @param array $params
     * @param string|null $formName
     * @return ActiveDataProvider
     */
    public function search(array $params = [], $formName = null)
    {
        $query = $this->getQuery();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => $this->getSort()
        ]);

        if (!$this->load($params, $formName) || !$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'client_email', $this->client_email]);
        $query->andFilterWhere(['like', 'delivery_at', $this->delivery_at]);
        $query->andFilterWhere([
            'partner_id' => $this->partner_id,
            'status' => $this->status
        ]);
        return $dataProvider;
    }
}
