<?php


namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;

/**
 * Class AbstractPersonSearch
 * @package app\models\search
 */
abstract class AbstractPersonSearch extends Model
{
    /**
     * @return ActiveRecord
     */
    abstract  public function getOwner(): ActiveRecord;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $email;

    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [
            [['name', 'email'], 'trim']
        ];
    }

    /**
     * @param array $params
     * @param string|null $formName
     * @return ActiveDataProvider
     */
    public function search(array $params = [], $formName = null)
    {
        $className = get_class($this->getOwner());
        $query = $className::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!$this->load($params, $formName) || !$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'name', $this->name]);
        $query->andFilterWhere(['like', 'email', $this->email]);
        return $dataProvider;
    }
}