<?php


namespace app\models\search;

use app\models\Product;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * Class ProductSearch
 * @package app\models\search
 */
class ProductSearch extends Model
{
    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $vendor_id;

    /**
     * @var float
     */
    public $price;

    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [
            [['name'], 'trim'],
            [['vendor_id'], 'integer'],
            [['price'], 'number'],
        ];
    }

    /**
     * @param array $params
     * @param string|null $formName
     * @return ActiveDataProvider
     */
    public function search(array $params = [], $formName = null)
    {
        $query = Product::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'name' => SORT_ASC
                ]
            ],
            'pagination' => [
                'pageSize' => 25
            ]
        ]);

        if (!$this->load($params, $formName) || !$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'name', $this->name]);
        $query->andFilterWhere(['like', 'price', $this->price]);
        $query->andFilterWhere([
            'vendor_id' => $this->vendor_id,
        ]);
        return $dataProvider;
    }
}
