<?php

namespace app\models;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * @property int $id
 * @property int $vendor_id
 * @property string $name
 * @property float $price
 * @property string $created_at
 * @property string $updated_at
 *
 * @property-read Vendor $vendor
 */
class Product extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%product}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vendor_id', 'name', 'price'], 'required'],
            [['vendor_id'], 'integer'],
            [['price'], 'number'],
            [['name'], 'string', 'max' => 255],
            [['vendor_id', 'name'], 'unique', 'targetAttribute' => ['vendor_id', 'name']],
            [['vendor_id'], 'exist', 'skipOnError' => true, 'targetRelation' => 'vendor'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'vendor_id' => Yii::t('app', 'Vendor'),
            'name' => Yii::t('app', 'Name'),
            'price' => Yii::t('app', 'Price'),
            'created_at' => Yii::t('app', 'Create date'),
            'updated_at' => Yii::t('app', 'Update date'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getVendor()
    {
        return $this->hasOne(Vendor::class, ['id' => 'vendor_id']);
    }
}
