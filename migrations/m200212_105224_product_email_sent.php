<?php

use yii\db\Migration;

/**
 * Class m200212_105224_product_email_sent
 */
class m200212_105224_product_email_sent extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%order}}', 'email_sent', $this->boolean()->notNull()->defaultValue(false));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%order}}', 'email_sent');
    }
}
