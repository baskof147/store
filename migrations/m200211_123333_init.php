<?php

use yii\db\Migration;

/**
 * Class m200211_123333_init
 */
class m200211_123333_init extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        //Vendor
        $this->createTable('{{%vendor}}', [
            'id' => $this->primaryKey(),
            'email' => $this->string()->notNull(),
            'name' => $this->string()->notNull(),
            'created_at' => $this->timestamp()->defaultExpression('now()'),
            'updated_at' => $this->timestamp()->defaultExpression('now()'),
        ]);
        $this->createIndex('uk_vendor_email', '{{%vendor}}', 'email', true);
        $this->createIndex('uk_vendor_name', '{{%vendor}}', 'name', true);

        //Partner
        $this->createTable('{{%partner}}', [
            'id' => $this->primaryKey(),
            'email' => $this->string()->notNull(),
            'name' => $this->string()->notNull(),
            'created_at' => $this->timestamp()->defaultExpression('now()'),
            'updated_at' => $this->timestamp()->defaultExpression('now()'),
        ]);
        $this->createIndex('uk_partner_email', '{{%partner}}', 'email', true);
        $this->createIndex('uk_partner_name', '{{%partner}}', 'name', true);

        //Product
        $this->createTable('{{%product}}', [
            'id' => $this->primaryKey(),
            'vendor_id' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'price' => $this->float()->notNull(),
            'created_at' => $this->timestamp()->defaultExpression('now()'),
            'updated_at' => $this->timestamp()->defaultExpression('now()'),
        ]);
        $this->createIndex('uk_product_vendor_name', '{{%product}}', [
            'vendor_id',
            'name'
        ], true);
        $this->addForeignKey(
            'fk_product_vendor_id',
            '{{%product}}',
            'vendor_id',
            '{{%vendor}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        //Order
        $this->createTable('{{%order}}', [
            'id' => $this->primaryKey(),
            'partner_id' => $this->integer()->notNull(),
            'status' => $this->integer()->notNull()->defaultValue(0),
            'client_email' => $this->string()->notNull(),
            'delivery_at' => $this->timestamp()->null(),
            'created_at' => $this->timestamp()->defaultExpression('now()'),
            'updated_at' => $this->timestamp()->defaultExpression('now()'),
        ]);
        $this->addForeignKey(
            'fk_order_partner_id',
            '{{%order}}',
            'partner_id',
            '{{%partner}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        //ProductOrder
        $this->createTable('{{%product_order}}', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer()->notNull(),
            'product_id' => $this->integer()->notNull(),
            'quantity' => $this->integer()->notNull(),
            'price' => $this->float()->notNull(),
            'created_at' => $this->timestamp()->defaultExpression('now()'),
            'updated_at' => $this->timestamp()->defaultExpression('now()'),
        ]);
        $this->addForeignKey(
            'fk_product_order_order_id',
            '{{%product_order}}',
            'order_id',
            '{{%order}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk_product_order_product_id',
            '{{%product_order}}',
            'product_id',
            '{{%product}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%product_order}}');
        $this->dropTable('{{%order}}');
        $this->dropTable('{{%product}}');
        $this->dropTable('{{%partner}}');
        $this->dropTable('{{%vendor}}');
    }
}
