<?php

use app\models\Order;
use app\models\ProductOrder;

/**
 * @var Order $model
 */

$totalSum = array_sum(array_map(function (ProductOrder $productOrder) {
    return $productOrder->price;
}, $model->productOrders));

foreach ($model->productOrders as $productOrder) { ?>
    <p>Product: <?= $productOrder->product->name ?>. Amount: <?= $productOrder->quantity ?>. Price: <?= $productOrder->price ?></p>
<?php } ?>
<p>Total sum: <?= $totalSum ?></p>
