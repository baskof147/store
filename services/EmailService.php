<?php

namespace app\services;

use app\models\Order;
use Yii;
use yii\base\BaseObject;

/**
 * Class EmailService
 * @package app\services
 */
class EmailService extends BaseObject
{
    /**
     * @param Order $model
     */
    public function sendOrderEmail(Order $model)
    {
        foreach ($this->getEmailsToSent($model) as $email) {
            Yii::$app->mailer->compose('order', [
                    'model' => $model
                ])
                ->setTo($email)
                ->setFrom([
                    Yii::$app->params['senderEmail'] => Yii::$app->params['senderName']]
                )
                ->setSubject(Yii::t('app', 'Order #{number} is done', [
                    'number' => $model->primaryKey
                ]))
                ->send()
            ;
        }
    }

    /**
     * @param Order $model
     * @return array
     */
    private function getEmailsToSent(Order $model)
    {
        $emails = [
            $model->partner->email
        ];
        foreach ($model->productOrders as $productOrder) {
            $emails[] = $productOrder->product->vendor->email;
        }
        return array_unique($emails);
    }
}
